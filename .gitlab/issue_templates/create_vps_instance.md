## Please provide the specifications that you require:

 * CPU: (eg, whether a single core would be fine, or if you'd really like as much as possible)
 * RAM: (2GB-16GB is usually manageable for a VPS, for more you might have to go through the DPL process)
 * Disk space: (most VPS instances come with 20GB by default, please specify how much you intend to use)
 * Desired hostname: (on debian.net, or elsewhere)
 * Public SSH key:

## Please provide details as applicable:

What will this instance be used for, who will be the target audience?

Who will maintain this service? Will it be team maintained?

What will you use for backups?

Will you be adding this service to https://wiki.debian.org/Services along with a service page under that name space? (this is not essential, but see this as a reminder to do so if appropriate for the service)

* [ ] I agree to follow the [Debian.net Rules](https://docs.debian.net/rules.html)

## Check-list for Debian.Net Team

* [ ] Select an appropriate platform for the request.
* [ ] Spin up a VM in the appropriate platform.
* [ ] Hetzner Only: Add IPv4 and IPv6 reverse-DNS entries.
* [ ] Add the Debian.Net Team's SSH keys to root's `authorized_keys`.
* [ ] Set `PasswordAuthentication no` in `sshd_config`, restart `sshd`.
* [ ] Add an account for the requester `adduser --disabled-password USER`.
* [ ] Grant sudo access to the requester `adduser USER sudo`.
* [ ] Unset the user's password, so they can set it themselves: `passwd -d USER`
* [ ] Add the requester's SSH keys to their account's `authorized_keys`.
* [ ] Provide IPv4 and IPv6 addresses in a comment, so the requester can create DNS records.
* [ ] Provide the host SSH fingerprint `ssh-keygen -l -f /etc/ssh/ssh_host_ed25519_key` in a comment.
* [ ] Add the instance to the [infrastructure repo](https://salsa.debian.org/debian.net-team/infra)

/label create-vps

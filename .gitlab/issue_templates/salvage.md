## What needs salvaging

 * **Hostname:** (which host needs to be salvaged)

(Describe the issue that needs to be fixed with the service.)

## Contact Attempts

(List the current maintainers of the service, according to the
 [infrastructure repo](https://salsa.debian.org/debian.net-team/infra))
(Describe attempts to contact the current maintainers of the service.)

## Next Steps

(List the team that will be salvaging the service. Provide public SSH
 keys.)
(Describe the next actions to salvage it.)

## Checklist for the Debian.net Team

- [ ] Verify issue.
- [ ] Make additional contact attempts.
- [ ] Wait 2 weeks, if possible.
- [ ] Take a backup of the instance, if possible.
- [ ] Verify that ssh password auth is disabled.
- [ ] Create accounts for the new team `adduser --disabled-password --add_extra_groups sudo`.
- [ ] Clear the passwords on the new accounts `passwd -d USER`.
- [ ] Add ssh keys to these accounts.
- [ ] Update the instance's owners in the infra repo.

/label salvage

## Describe the host

 * **Hostname:** (which host is to be retired)

(Describe the reason for retirement.)

## Pre-retirement actions

(Does anything need a final backup/migration before retirement? Where?)

## Shut-down checklist for the Debian.net Team

- [ ] Final backup (if necessary).
- [ ] Shut down instance.
- [ ] Remove any volumes associated with the instance (if necessary).
- [ ] Remove instance from infra repo.
- [ ] Ask requester to remove hostname from DNS

/label retirement
